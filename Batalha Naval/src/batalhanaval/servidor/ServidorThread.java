package batalhanaval.servidor;

import batalhanaval.modelos.Room;
import java.awt.Point;
import java.io.*;
import java.net.*;
import java.util.Random;

/**
 *
 * @author Douglas
 */
public class ServidorThread extends Thread {

    private Socket conexao;
    private String nome;
    private ServidorBatalhaNaval servidorBatalhaNaval;
    private BufferedReader doCliente;
    private DataOutputStream paraCliente;
    private Room minhaSala = new Room();
    private int meuMapa[][];

    public ServidorThread(Socket conexao, ServidorBatalhaNaval servidorBatalhaNaval) {
        try {
            this.conexao = conexao;
            this.servidorBatalhaNaval = servidorBatalhaNaval;
            this.doCliente = new BufferedReader(
                    new InputStreamReader(conexao.getInputStream()));
            this.paraCliente = new DataOutputStream(
                    conexao.getOutputStream());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void notificar(String mensagem) {
        try {
            paraCliente.writeBytes(mensagem);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void configurarMapa() {
        Random gerador = new Random();

        int mapa1[][] = {
            {1, 1, 1, 0, 0, 1, 0, 0, 0, 0},
            {0, 1, 0, 0, 0, 0, 0, 0, 1, 1},
            {0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 1, 1, 0, 0, 0},
            {0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 1, 0, 0, 1, 1, 1, 0, 0, 0},
            {0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 1, 0, 0, 0},
            {1, 1, 1, 1, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 1}
        };

        int mapa2[][] = {
            {0, 0, 0, 1, 1, 0, 1, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 1, 0, 0, 1, 1, 0, 0, 1},
            {0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 1, 0, 0, 0, 1, 0},
            {0, 0, 0, 0, 1, 0, 0, 0, 1, 0},
            {0, 0, 0, 0, 0, 0, 0, 1, 1, 1},
            {1, 1, 1, 1, 0, 0, 0, 0, 0, 0}
        };

        int mapa3[][] = {
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 1, 0, 0, 0, 1, 0, 0},
            {0, 0, 0, 1, 0, 0, 0, 1, 0, 1},
            {0, 0, 1, 1, 1, 0, 0, 0, 0, 1},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 1, 0, 0, 0, 0, 0, 0, 1, 0},
            {0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 1, 0, 0, 1, 1, 1, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {1, 0, 1, 1, 1, 1, 0, 0, 0, 1}
        };

        int r = gerador.nextInt(4);
        int[][] mapa = mapa1.clone();

        if (r % 3 == 0) {
            mapa = mapa1.clone();
        }
        if (r % 3 == 1) {
            mapa = mapa2.clone();
        }
        if (r % 3 == 2) {
            mapa = mapa3.clone();
        }

        this.meuMapa = mapa;
        this.minhaSala = servidorBatalhaNaval.getRoom(this.minhaSala);
        this.minhaSala.setMapa(mapa, this);
        servidorBatalhaNaval.getRoom(this.minhaSala).setMapa(mapa, this);
    }

    public void imprimirMapa() {
        try {
            for (int i = 0; i < this.meuMapa.length; i++) {
                for (int j = 0; j < this.meuMapa.length; j++) {
                    switch (this.meuMapa[i][j]) {
                        case 0:
                            paraCliente.writeBytes("?");
                            break;
                        case 1:
                            paraCliente.writeBytes("#");
                            break;
                        default:
                            break;
                    }
                }
                paraCliente.writeBytes("\r\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void imprimirMapaCodificado(int mapa[][]) {
        try {
            for (int i = 0; i < mapa.length; i++) {
                for (int j = 0; j < mapa.length; j++) {
                    switch (mapa[i][j]) {
                        case 0:
                        case 1:
                            paraCliente.writeBytes("?");
                            break;
                        case 2:
                            paraCliente.writeBytes("#");
                            break;
                        case 3:
                            paraCliente.writeBytes("*");
                            break;
                        default:
                            break;
                    }
                }
                paraCliente.writeBytes("\r\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Point escolherXY() {
        Point p;
        String x, y;
        try {
            paraCliente.writeBytes("Escolha a posicao X e Y do mapa para lancar a bomba!!\r\n");
            paraCliente.writeBytes("Posicao X: ");
            x = doCliente.readLine();
            paraCliente.writeBytes("Posicao Y: ");
            y = doCliente.readLine();

            p = new Point(Integer.parseInt(x), Integer.parseInt(y));

        } catch (Exception e) {
            p = new Point();
            e.printStackTrace();
        }

        return p;
    }

    @Override
    public void run() {
        try {
            this.paraCliente.writeBytes("Forneca seu nome: ");
            this.nome = this.doCliente.readLine();

            while (true) {
                paraCliente.writeBytes("Comandos: /listar, /criar, /rooms, "
                        + "/entrar, /iniciar (apenas se estiver na room)\r\n");

                String comando = doCliente.readLine();
                switch (comando) {
                    case "/listar":
                        for (ServidorThread servidorThread : servidorBatalhaNaval.getServidores()) {
                            paraCliente.writeBytes(servidorThread.nome + "\r\n");
                        }
                        break;
                    case "/criar":
                        this.minhaSala = servidorBatalhaNaval.createRoom(this, "Sala do " + this.nome);
                        break;
                    case "/rooms":
                        for (Room room : servidorBatalhaNaval.getRooms()) {
                            paraCliente.writeBytes(room.getNomeRoom() + "\r\n");
                        }
                        break;
                    case "/entrar":
                        this.paraCliente.writeBytes("Escreva o nome da room que deseja entrar: ");
                        String roomDesejada = this.doCliente.readLine();

                        int i = 0;
                        for (Room room : servidorBatalhaNaval.getRooms()) {
                            if (roomDesejada.equals(room.getNomeRoom())) {
                                room.setAdversario(this);
                                this.minhaSala = room;
                                this.paraCliente.writeBytes("Voce entrou na sala de " + room.getDono().getNome() + "!\r\n");
                            } else {
                                i++;
                            }
                        }
                        if (i == servidorBatalhaNaval.getRooms().size()) {
                            this.paraCliente.writeBytes("Sala nao encontrada!\r\n");
                        }
                        break;
                    case "/iniciar":
                        if (!this.minhaSala.getNomeRoom().isEmpty()) {
                            servidorBatalhaNaval.getRoom(this.minhaSala).setStatusJogador(1, this);
                            if (servidorBatalhaNaval.getRoom(this.minhaSala).getStatusDono() == 0 || servidorBatalhaNaval.getRoom(this.minhaSala).getStatusAdversario() == 0) {
                                servidorBatalhaNaval.novoJogo(this.minhaSala);
                            }
                            //this.paraCliente.writeBytes(servidorBatalhaNaval.getRoom(this.minhaSala).getStatusJogo() + "\r\n");

                            while (true) {
                                if (servidorBatalhaNaval.getRoom(minhaSala).getStatusJogo() == 2) {
                                    paraCliente.writeBytes("Jogo encerrado!!!!! Obrigado!!\\r\\n");
                                    break;
                                }
                            }
                        } else {
                            this.paraCliente.writeBytes("Impossivel iniciar, voce nao se "
                                    + "encontra em uma sala.\r\n");
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getNome() {
        return nome;
    }

    public Socket getConexao() {
        return conexao;
    }
}
