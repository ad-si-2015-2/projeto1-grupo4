package batalhanaval.servidor;

import batalhanaval.modelos.Room;
import java.awt.Point;
import java.net.*;
import java.util.ArrayList;

/**
 *
 * @author Douglas
 */
public class ServidorBatalhaNaval {

    private ServerSocket conexaoServidor;
    private ArrayList<Room> rooms = new ArrayList<>();
    private ArrayList<ServidorThread> servidores = new ArrayList<>();

    public ServidorBatalhaNaval() {
        try {
            this.conexaoServidor = new ServerSocket(1234);
            System.out.println("Esperando conexoes na porta " + 1234);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Room createRoom(ServidorThread dono, String nomeRoom) {
        Room room = new Room(dono, nomeRoom);
        this.getRooms().add(room);
        dono.notificar("Sala criada com sucesso!\n");
        return room;
    }

    public Room getRoom(Room room) {
        for (Room r : this.rooms) {
            if (r.equals(room)) {
                return r;
            }
        }
        return null;
    }

    public void novoJogo(Room r) throws InterruptedException {
        Room room = this.searchRoom(r);
        Boolean b = false;

        while (room.getStatusJogador(room.getDono()) == 0 || room.getStatusJogador(room.getAdversario()) == 0) {
            if (!b) {
                if (room.getStatusDono() == 0) {
                    room.getDono().notificar("Aguardando voce iniciar o JOGO!!\n");
                }
                if (room.getStatusAdversario() == 0) {
                    room.getAdversario().notificar("Aguardando voce iniciar o JOGO!!\n");
                }
                b = true;
            }
            Thread.sleep(5 * 1000);
        }

        this.searchRoom(r).setStatusJogo(1); //Jogo inicializado
        this.searchRoom(r).setRodada(1);

        room.getDono().notificar("JOGO INICIADO!!\n");
        room.getAdversario().notificar("JOGO INICIADO!!\n");

        //Configurar mapa
        room.getDono().notificar("Aguardando voce configurar o mapa!\r\n");
        room.getDono().configurarMapa();
        room.getDono().notificar("Mapa configurado, seu mapa e esse:\r\n");
        room.getDono().imprimirMapa();

        room.getAdversario().notificar("Aguardando voce configurar o mapa!\r\n");
        room.getAdversario().configurarMapa();
        room.getDono().notificar("Mapa configurado, seu mapa e esse:\r\n");
        room.getAdversario().imprimirMapa();

        while (this.searchRoom(r).getStatusJogo() != 0) {
            if (this.searchRoom(r).getStatusJogo() == 2) {
                break;
            }

            //System.out.println("Atual: " + "Objeto: " + this.searchRoom(r).getRodada() + "\r\n");
            if (!this.novaRodada(room)) {
                this.searchRoom(r).setStatusJogo(2); //Jogo finalizado
                break;
            }
        }
    }

    public Room searchRoom(Room room) {
        for (Room r : this.getRooms()) {
            if (room.getNomeRoom().equals(r.getNomeRoom())) {
                return r;
            }
        }

        return null;
    }

    public Boolean novaRodada(Room room) {
        Point p;
        //System.out.println("Nova rodada iniciada");
        if (this.searchRoom(room).getRodada() % 2 == 1) {
            //room.getAdversario().notificar("Rodada: " + this.searchRoom(room).getRodada());
            p = room.getDono().escolherXY();
            if(p.x < 0 || p.x > 9 || p.y <0 || p.y > 9){
                room.getDono().notificar("Voce escolheu uma posicao inexistente!!!\r\n");
                p = room.getDono().escolherXY();
            }
            room.getDono().notificar("\r\n");

            int mapaAdversario[][] = room.getMapaAdversario();
            if (mapaAdversario[p.x][p.y] == 1) {
                mapaAdversario[p.x][p.y] = 2;
                this.searchRoom(room).setMapaAdversario(mapaAdversario);

                room.getAdversario().notificar("KABUUUUUUMM!!! O INIMIGO TE ACERTOOU!!\r\n");
                room.getDono().notificar("VOCE ACERTOU ELE!!!\r\n");
                room.getDono().imprimirMapaCodificado(mapaAdversario);

                //Verfica se o jogo acabou
                if (isJogoAcabado(mapaAdversario)) {
                    return false;
                }
            } else if (mapaAdversario[p.x][p.y] == 0) {
                mapaAdversario[p.x][p.y] = 3;
                this.searchRoom(room).setMapaAdversario(mapaAdversario);

                room.getAdversario().notificar("PLUFT!!! O INIMIGO ERROU!!\r\n");
                room.getDono().notificar("VOCE ERROU!!!\r\n");
                room.getDono().imprimirMapaCodificado(mapaAdversario);
            }

            this.searchRoom(room).setRodada(this.searchRoom(room).getRodada() + 1);
        } else {
            p = room.getAdversario().escolherXY();
            if(p.x < 0 && p.x > 9 && p.y <0 && p.y > 9){
                room.getAdversario().notificar("Voce escolheu uma posicao inexistente!!!\r\n");
                p = room.getAdversario().escolherXY();
            }
            room.getDono().notificar("\r\n");

            int mapaDono[][] = room.getMapaDono();
            if (mapaDono[p.x][p.y] == 1) {
                mapaDono[p.x][p.y] = 2;
                this.searchRoom(room).setMapaDono(mapaDono);

                room.getDono().notificar("KABUUUUUUMM!!! O INIMIGO TE ACERTOOU!!\r\n");
                room.getAdversario().notificar("VOCE ACERTOU ELE!!!\r\n");
                room.getAdversario().imprimirMapaCodificado(mapaDono);

                //Verfica se o jogo acabou
                if (isJogoAcabado(mapaDono)) {
                    return false;
                }
            } else if (mapaDono[p.x][p.y] == 0) {
                mapaDono[p.x][p.y] = 3;
                this.searchRoom(room).setMapaDono(mapaDono);

                room.getDono().notificar("PLUFT!!! O INIMIGO ERROU!!\r\n");
                room.getAdversario().notificar("VOCE ERROU!!!\r\n");
                room.getAdversario().imprimirMapaCodificado(mapaDono);
            }

            this.searchRoom(room).setRodada(this.searchRoom(room).getRodada() + 1);
        }

        return true;
    }

    public Boolean isJogoAcabado(int mapa[][]) {
        int cont = 0;

        for (int i = 0; i < mapa.length; i++) {
            for (int j = 0; j < mapa.length; j++) {
                if (mapa[i][j] == 2) {
                    cont++;
                }
            }
        }

        if (cont == 22) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        ServidorBatalhaNaval servidorBatalhaNaval = new ServidorBatalhaNaval();

        while (true) {
            Socket conexao = servidorBatalhaNaval.conexaoServidor.accept();
            ServidorThread st = new ServidorThread(conexao, servidorBatalhaNaval);
            servidorBatalhaNaval.getServidores().add(st);
            st.start();
        }
    }

    public ArrayList<Room> getRooms() {
        return rooms;
    }

    public ArrayList<ServidorThread> getServidores() {
        return servidores;
    }
}
