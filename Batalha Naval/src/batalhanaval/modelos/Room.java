package batalhanaval.modelos;

import batalhanaval.servidor.ServidorThread;

/**
 *
 * @author Douglas
 */
public class Room {

    private ServidorThread dono;
    private ServidorThread adversario;
    private String nomeRoom;

    private int statusJogo;
    private int statusDono;
    private int statusAdversario;

    private int mapaDono[][];
    private int mapaAdversario[][];

    private int rodada;
    
    private Boolean donoJogou;
    private Boolean adversarioJogou;

    public Room() {
        this.nomeRoom = "";
    }

    public Room(ServidorThread dono, String nomeRoom) {
        this.dono = dono;
        this.nomeRoom = nomeRoom;
        this.statusJogo = 0;
        this.statusDono = 0;
        this.statusAdversario = 0;
        this.mapaDono = new int[10][10];
        this.mapaAdversario = new int[10][10];
        this.rodada = 0;
    }

    public ServidorThread getDono() {
        return dono;
    }

    public ServidorThread getAdversario() {
        return adversario;
    }

    public void setAdversario(ServidorThread adversario) {
        this.adversario = adversario;
    }

    public String getNomeRoom() {
        return nomeRoom;
    }

    public void setNomeRoom(String nomeRoom) {
        this.nomeRoom = nomeRoom;
    }

    public int getStatusJogo() {
        return statusJogo;
    }

    public void setStatusJogo(int statusJogo) {
        this.statusJogo = statusJogo;
    }

    public int[][] getMapa(ServidorThread t) {
        if (t.getNome().equals(this.dono.getNome())) {
            return mapaDono;
        } else {
            return mapaAdversario;
        }
    }

    public void setMapa(int[][] mapa, ServidorThread t) {
        if (t.getNome().equals(this.dono.getNome())) {
            this.mapaDono = mapa;
        } else {
            this.mapaAdversario = mapa;
        }
    }

    public int getStatusJogador(ServidorThread t) {
        if (t.getNome().equals(this.dono.getNome())) {
            return statusDono;
        } else {
            return statusAdversario;
        }
    }

    public void setStatusJogador(int status, ServidorThread t) {
        if (t.getNome().equals(this.dono.getNome())) {
            this.statusDono = status;
        } else {
            this.statusAdversario = status;
        }
    }

    public int getRodada() {
        return rodada;
    }

    public void setRodada(int rodada) {
        this.rodada = rodada;
    }

    public int getStatusDono() {
        return statusDono;
    }

    public int getStatusAdversario() {
        return statusAdversario;
    }

    public Boolean getDonoJogou() {
        return donoJogou;
    }

    public void setDonoJogou(Boolean donoJogou) {
        this.donoJogou = donoJogou;
    }

    public Boolean getAdversarioJogou() {
        return adversarioJogou;
    }

    public void setAdversarioJogou(Boolean adversarioJogou) {
        this.adversarioJogou = adversarioJogou;
    }

    public int[][] getMapaDono() {
        return mapaDono;
    }

    public int[][] getMapaAdversario() {
        return mapaAdversario;
    }

    public void setMapaDono(int[][] mapaDono) {
        this.mapaDono = mapaDono;
    }

    public void setMapaAdversario(int[][] mapaAdversario) {
        this.mapaAdversario = mapaAdversario;
    }
    
    

}
